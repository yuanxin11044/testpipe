package org.example.demo;

import org.junit.Assert;
import org.junit.Test;

public class Demo {

    @Test
    public void assertTitle(){
        String currentTitle = "RV";
        String expectedTitle = "RV";

        Assert.assertEquals(expectedTitle, currentTitle);
    }
}
